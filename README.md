This script automates all the steps required to migrate from any Ubuntu version to its Trisquel counterpart without reinstalling the system. It will replace the kernel with Linux-libre, remove as many non-free packages as possible and install the default Trisquel package set.

Just copy the script into a file, call it trisquelize.sh and run.

    sudo sh trisquelize.sh

## Notes
Edit the follwing variables as needed.

    $MIRROR, $RELEASE, $EDITION
	
* The sample contents of those variables are intended to migrate from Ubuntu 12.04 Precise to Trisquel 6.0.
*  Never attempt to run this script from a Ubuntu edition with no Trisquel counterpart! Ask in the forums if you are in doubt.
* The script attempts to remove a set of known non-free packages available in Ubuntu main and universe repositories; everything else will be preserved.
* If your disk is encrypted make sure you have the plaintext key written down in case something goes wrong.
* Making backups is recommended.
* The script has some long lines; mind them if you modify it.
* If something goes wrong and you need help, paste the contents of `/var/log/trisquelize.log` in http://trisquel.pastebin.com and ask in the forum posting the link to your log at pastebin.
* We recommend you to join the #trisquel IRC channel at irc.freenode.org during the process, in case you ever need help.

PS: This text has been originally copied from https://trisquel.info/en/wiki/migrate-ubuntu-trisquel-without-reinstalling
